import React, { useState, useEffect } from 'react';
import { useHistory, Link } from 'react-router-dom'
import ModalForm from '../../shared/newPatientModal';
import UpdateModalForm from '../../shared/updatePatientModal';
import Axios from 'axios';

const SearchAdmin = (props) => {

    // let id = props.match.params.id;

    let history = useHistory();

    const [patientList, setPatientList] = useState([])

    useEffect(() => {
        Axios.get('http://localhost:3005/getpatients').then((response) => {
            setPatientList(response.data)
        })
    }, [])

    const patientRoute = (event, patient) =>{
      // prevent default button behaviour
        event.preventDefault();
        let path = `/updatePatient/${patient.id}`;
        history.push(path);
    };


    const [searchTerm, setsearchTerm] = useState("")


    document.querySelectorAll('#searchTable button.btn.btn-primary').forEach(function(element){
            element.addEventListener('click', function(e){
                let row = this.closest('tr');
                let rowID = row.cells[0].textContent;

                console.log(rowID)
            })
        })


        const deleteRecord = (id) => {
            Axios.delete(`http://localhost:3005/deletepatient/${id}`)
        };

    return(
        <div className="containerMain">


           {/*search container */}
            <div className="searchContainer">

            {/*search input */}
                <div className="searchInput">

                    <h1 className="header">Patients</h1>    

                    <div className="newPatient">
                        <ModalForm />
                    </div>


                    <input 
                    type="text" 
                    placeholder="Patient Search" 
                    className="form-control" 
                    onChange = {(e)=>{
                        setsearchTerm(e.target.value);
                    }}
                    ></input>
                </div>

            {/* patient data table */}

                {patientList.filter((val) => {
                        if (searchTerm === "") {
                            return val;
                        } else if (
                            val.name
                            .toLowerCase()
                            .includes(searchTerm.toLowerCase()) ||
                            val.email.toLowerCase().includes(searchTerm.toLowerCase())
                        ) {
                            return val;
                        }
                        }).map((val) => {
                    return (
                        <table className="tableSearch" id="searchTable">
                            <thead className="tableHeadSA">
                                <tr>
                                    <th className="head1">ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Address</th>
                                    <th>City</th>
                                    <th>Province</th>
                                    <th>Postal Code</th>
                                    <th>DOB</th>
                                    <th>gender</th>
                                    <th>Blood Type</th>
                                    <th>Health Card #</th>
                                    <th className="head2">Edit</th>
                                    <th className="head2">Delete</th>
                                    <th className="head2">Select</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{val.id}</td>
                                    <td>{val.name}</td>
                                    <td>{val.email}</td>
                                    <td>{val.address}</td>
                                    <td>{val.city}</td>
                                    <td>{val.province}</td>
                                    <td>{val.postal_code}</td>
                                    <td>{val.DOB}</td>
                                    <td>{val.Gender}</td>
                                    <td>{val.blood_type}</td>
                                    <td>{val.health_card_number}</td>
                                    <td>
                                        {/* <Link to={"/updatePatient/"+val.id}>
                                            <button className="btn btn-primary" >Edit</button> */}
                                            <UpdateModalForm onClick={(event) => patientRoute(event, val)}/>
                                        {/* </Link> */}
                                    </td>
                                    <td>
                                        <button className="btn btn-primary" onClick={() => {deleteRecord(val.id)}}>Delete</button>
                                    </td>
                                    <td>
                                        <Link to={"/patientDetails/"+val.id}>
                                            <button className="btn btn-primary" >Select</button>
                                        </Link>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    )
                })}

            </div>
        </div>
    );

}

export default SearchAdmin;
