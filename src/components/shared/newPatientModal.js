import React, { useState } from 'react';
import { Button, Modal, ModalHeader, ModalBody } from 'reactstrap';
import NewPatientForm from './newPatientForm';

const ModalForm = (props) => {
 
    const {
    className
  } = props;

  const [modal, setModal] = useState(false);

//   
  const toggle = () => setModal(!modal);

  return (
    <div>
      <Button color="danger" onClick={toggle}>New Patient</Button>
      <Modal isOpen={modal} toggle={toggle} className={className}>
        <ModalHeader toggle={toggle}>Create New Patient</ModalHeader>
        
        <ModalBody> 
          
          <NewPatientForm />
        
        </ModalBody>

       
      </Modal>
    </div>
  );
}

export default ModalForm;