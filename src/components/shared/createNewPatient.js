import React, { useState, useEffect } from 'react';
import Axios from 'axios';

const NewPatientForm = () => {

    const [fullName, setFullName] = useState('');
    const [email, setEmail] = useState('');
    const [address, setAddress] = useState('');
    const [city, setCity] = useState('');
    const [province, setProvince] = useState('');
    const [postalCode, setPostalCode] = useState('');
    const [healthCard, setHealthCard] = useState('');
    const [bloodType, setBoolType] = useState('');
    const [dob, setDOB] = useState('');
    const [gender, setGender] = useState('');
    // const [permission, setPermission] = useState('');

    const createNewPatient = () => {
        Axios.post('http://localhost:3002/createNewPatient', {
            name: fullName,
            email: email,
            address: address,
            city: city,
            province: province,
            postal_code: postalCode,
            health_card_number: healthCard,
            blood_type: bloodType,
            DOB: dob,
            Gender: gender,
            // permission: permission
        }).then(() => {
            alert("Patient Created Successfully")
        });
    };

    
    return(
        
        <main className="newpatient">
            <h1>Create New Patient</h1>
            <div className="newpatientcomponent">
                <label>Full Name</label>
                <input 
                        type="text"
                        name="patientName" 
                        id="patientName" 
                        onChange={(e) => {
                            setFullName(e.target.value)
                        }}
                />

            <label>Email</label>
                    <input 
                        type="email" 
                        name="email"
                        id="exampleEmail" 
                        onChange={(e) => {
                            setEmail(e.target.value)
                        }}
                    />

                    <label>Address</label>
                    <input 
                        type="text" 
                        name="patientAdd"
                        id="patientAdd" 
                        onChange={(e) => {
                            setAddress(e.target.value)
                        }}
                    />

                    <label>City</label>
                    <input 
                        type="text"
                        name="patientCity"
                        id="patientCity" 
                        onChange={(e) => {
                            setCity(e.target.value)
                        }}
                    />

                    <label>Province</label>
                    <input 
                        type="text" 
                        name="patientProv" 
                        id="patientProv" 
                        onChange={(e) => {
                            setProvince(e.target.value)
                        }}
                    />

                    <label>Postal Code</label>
                    <input 
                        type="text" 
                        name="patientPC"
                        id="patientPC" 
                        onChange={(e) => {
                            setPostalCode(e.target.value)
                        }}
                    />

                    <label>Health Card</label>
                    <input 
                        type="text" 
                        name="patientHC" 
                        id="patientHC"
                        onChange={(e) => {
                            setHealthCard(e.target.value)
                        }}
                    />

                    <label>Blood Type</label>
                    <input 
                        type="text" 
                        name="patientBlood" 
                        id="patientBlood"
                        onChange={(e) => {
                            setBoolType(e.target.value)
                        }}
                    />

                    <label>Date of Birth</label>
                    <input 
                        type="date"
                        name="patientDOB"
                        id="patientDOB" 
                        onChange={(e) => {
                            setDOB(e.target.value)
                        }}
                    />

                    <label>Gender</label>
                    <input 
                        type="text"
                        name="gender"
                        id="patientDOB" 
                        onChange={(e) => {
                            setGender(e.target.value)
                        }}
                    />

                    {/* <label>Permission</label>
                    <input 
                        type="text"
                        name="permission"
                        id="patientDOB" 
                        onChange={(e) => {
                            setPermission(e.target.value)
                        }} 
                    /> */}
                        <button onClick={createNewPatient}>Submit</button>
            </div>
        </main>
    )

}

export default NewPatientForm

            