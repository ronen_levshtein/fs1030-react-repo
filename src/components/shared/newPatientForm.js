import React, { useState } from 'react';
import Axios from 'axios';
import { Form, FormGroup, Label, Input } from 'reactstrap';

const NewPatientForm = () => {

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [address, setAddress] = useState('');
    const [city, setCity] = useState('');
    const [province, setProvince] = useState('');
    const [postal_code, setPostalCode] = useState('');
    const [health_card_number, setHealthCard] = useState('');
    const [blood_type, setBloodType] = useState('');
    const [dob, setDOB] = useState('');
    const [Gender, setGender] = useState('');


    const createNewPatient = () => {
        Axios.post('http://localhost:3005/insertinto', {
            name: name,
            email: email,
            address: address,
            city: city,
            province: province,
            postal_code: postal_code,
            health_card_number: health_card_number,
            blood_type: blood_type,
            DOB: dob,
            Gender: Gender,
        }).then(() => {
            alert("Patient Created Successfully")
        });
    };
    
    return(
        

        <Form>
                <FormGroup>
                    <Label for="patientName">Full Name</Label>
                    <Input 
                        type="text"
                        id="patientName" 
                        onChange={(e) => {
                            setName(e.target.value)
                        }}
                    
                    />
                </FormGroup>
                <FormGroup>
                    <Label for="exampleEmail">Email</Label>
                    <Input 
                        type="email" 
                        id="exampleEmail" 
                        onChange={(e) => {
                            setEmail(e.target.value)
                        }}
                    
                    />
                </FormGroup>
                <FormGroup>
                    <Label for="patientAdd">Address</Label>
                    <Input 
                        type="text" 
                        id="patientAdd" 
                        onChange={(e) => {
                            setAddress(e.target.value)
                        }}
            
                    />
                </FormGroup>
                <FormGroup>
                    <Label for="patientCity">City</Label>
                    <Input 
                        type="text"
                        id="patientCity" 
                        onChange={(e) => {
                            setCity(e.target.value)
                        }}
                
                    />
                </FormGroup>
                <FormGroup>
                    <Label for="patientProv">Province</Label>
                    <Input 
                        type="text" 
                        id="patientProv" 
                        onChange={(e) => {
                            setProvince(e.target.value)
                        }}

                    />
                </FormGroup>
                <FormGroup>
                    <Label for="patientPC">Postal Code</Label>
                    <Input 
                        type="text" 
                        id="patientPC" 
                        onChange={(e) => {
                            setPostalCode(e.target.value)
                        }}
                    
                    />
                </FormGroup>
                <FormGroup>
                    <Label for="patientHC">Health Card</Label>
                    <Input 
                        type="text" 
                        id="patientHC"
                        onChange={(e) => {
                            setHealthCard(e.target.value)
                        }}
                    
                    />
                </FormGroup>
                <FormGroup>
                    <Label for="patientBlood">Blood Type</Label>
                    <Input 
                        type="text" 
                        id="patientBlood"
                        onChange={(e) => {
                            setBloodType(e.target.value)
                        }}
                    
                    />
                </FormGroup>
                <FormGroup>
                    <Label for="patientDOB">Date of Birth</Label>
                    <Input 
                        type="date"
                        id="patientDOB" 
                        onChange={(e) => {
                            setDOB(e.target.value)
                        }}
                
                    />
                </FormGroup>
                <FormGroup>
                    <Label for="patientDOB">Gender</Label>
                    <Input 
                        type="text"
                        id="patientDOB" 
                        onChange={(e) => {
                            setGender(e.target.value)
                        }}
            
                    />
                </FormGroup>
        
                        <button onClick={createNewPatient}>Submit</button>
                </Form>

    )

}

export default NewPatientForm

            