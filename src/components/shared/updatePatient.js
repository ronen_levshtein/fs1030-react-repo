import axios from 'axios';
import React, { useState, useEffect } from 'react';
import { Form, FormGroup, Label, Input } from 'reactstrap';

const UpdatePatientForm = (props) => {

    console.warn("props", props.match.params.id)

    let id = props.match.params.id;

    console.log(id);
    

    const [patients, setPatients] = useState([]);

    useEffect(()=> {
        async function fetchData() {
            const res = await fetch(`http://localhost:3005/updatepatient/${id}`);
            res
            .json()
            .then((res) => setPatients(res))
            .catch((err) => console.log(err))
        }
        fetchData();
    }, [id])  


    const [newName, setNewName] = useState('');
    const [newEmail, setNewEmail] = useState('');
    const [newAddress, setNewAddress] = useState('');
    const [newCity, setNewCity] = useState('');
    const [newProvince, setNewProvince] = useState('');
    const [newPostal_code, setNewPostalCode] = useState('');
    const [newHealth_card_number, setNewHealthCard] = useState('');
    const [newBlood_type, setNewBloodType] = useState('');
    const [newDob, setNewDOB] = useState('');
    const [newGender, setNewGender] = useState('');

    const updatePatient = (id) => {
        axios.put("http://localhost:3005/updatepatient", {
            id: id,
            name: newName,
            email: newEmail,
            address: newAddress,
            city: newCity,
            province: newProvince,
            postal_code: newPostal_code,
            health_card_number: newHealth_card_number,
            blood_type: newBlood_type,
            DOB: newDob,
            Gender: newGender,
        });
        setNewName('');
        setNewEmail('');
        setNewAddress('');
        setNewCity('');
        setNewProvince('');
        setNewPostalCode('');
        setNewHealthCard('');
        setNewBloodType('');
        setNewDOB('');
        setNewGender('');
    }

    return(
        

        <Form>
            {patients.map((patient) => (
                    <div key={patient.id}>
                        <FormGroup>
                            <Label for="patientName">Full Name</Label>
                            <Input 
                                type="text"
                                id="patientName" 
                                defaultValue={patient.name}
                                onChange={(e) => {
                                    setNewName(e.target.value)
                                }}
                                            
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="exampleEmail">Email</Label>
                            <Input 
                                id="exampleEmail" 
                                type="email" 
                                defaultValue={patient.email}
                                onChange={(e) => {
                                    setNewEmail(e.target.value)
                                }}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="patientAdd">Address</Label>
                            <Input 
                                type="text" 
                                id="patientAdd" 
                                defaultValue={patient.address}
                                onChange={(e) => {
                                    setNewAddress(e.target.value)
                                }}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="patientCity">City</Label>
                            <Input 
                                type="text"
                                id="patientCity" 
                                defaultValue={patient.city}
                                onChange={(e) => {
                                    setNewCity(e.target.value)
                                }}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="patientProv">Province</Label>
                            <Input 
                                type="text" 
                                id="patientProv" 
                                defaultValue={patient.province}
                                onChange={(e) => {
                                    setNewProvince(e.target.value)
                                }}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="patientPC">Postal Code</Label>
                            <Input 
                                type="text" 
                                id="patientPC" 
                                defaultValue={patient.postal_code}
                                onChange={(e) => {
                                    setNewPostalCode(e.target.value)
                                }}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="patientHC">Health Card</Label>
                            <Input 
                                type="text" 
                                id="patientHC"
                                defaultValue={patient.health_card_number}
                                onChange={(e) => {
                                    setNewHealthCard(e.target.value)
                                }}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="patientBlood">Blood Type</Label>
                            <Input 
                                type="text" 
                                id="patientBlood"
                                defaultValue={patient.blood_type}
                                onChange={(e) => {
                                    setNewBloodType(e.target.value)
                                }}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="patientDOB">Date of Birth</Label>
                            <Input 
                                type="date"
                                id="patientDOB" 
                                defaultValue={patient.dob}
                                onChange={(e) => {
                                    setNewDOB(e.target.value)
                                }}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="patientDOB">Gender</Label>
                            <Input 
                                type="text"
                                id="patientDOB" 
                                defaultValue={patient.Gender}
                                onChange={(e) => {
                                    setNewGender(e.target.value)
                                }}
                            />
                        </FormGroup>
                                
                        <button onClick={updatePatient(id)}>Submit</button>
                    </div>
            ))}
                
                </Form>
    )
}

export default UpdatePatientForm

            