import React, { useState } from 'react';
import { Button, Modal, ModalHeader, ModalBody } from 'reactstrap';
import UpdatePatientForm from './updatePatientForm';

const UpdateModalForm = (props) => {

    const {
    className
  } = props;

  const [modal, setModal] = useState(false);

//   
  const toggle = () => setModal(!modal);

  return (
    <div className="updateModal">
      <Button className="btn btn-primary" onClick={toggle}>Edit</Button>
      <Modal isOpen={modal} toggle={toggle} contentClassName="custom-modal-style" className={className}>
        <ModalHeader toggle={toggle}>Update PatientDetails</ModalHeader>
        
        <ModalBody> 
          
          <UpdatePatientForm />
        
        </ModalBody>

      </Modal>
    </div>
  );
}

export default UpdateModalForm;